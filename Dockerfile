FROM nginx
MAINTAINER canknow <3230525823@qq.com>

ENV RUN_USER nginx
ENV RUN_GROUP nginx
ENV LOG_DIR /data/log/nginx

ADD nginx/html /var/www/html/website
ADD nginx/conf.d/global.conf /etc/nginx/conf.d/
ADD nginx/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
